package test;

import com.opencsv.CSVWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;

/**
 * Класс создания файла
 *
 * @author Nold
 */
public class GenFile {

    /**
     * Метод создания csv файла. Получает строки из GenString и записывает в
     * файл. Для форматирования строк в csv формат используется opencsv
     *
     * @param paramlist параметры полученные из командой строки
     */
    public static void genFile(Param paramlist) throws IOException, Exception {

        try (CSVWriter writer = new CSVWriter(new FileWriter(paramlist.getOut()), ';');) {
            GenString gs = new GenString();

            writer.writeNext(gs.getTitleRow(paramlist.getCol()));
            for (int i = 0; i < paramlist.getRow(); i++) {
                writer.writeNext(gs.getRndRow(paramlist));
            }

        } catch (IOException ex) {
            throw new IOException("Ошибка создания файла с указанным именем");
        } catch (ParseException ex) {
            throw new Exception("Ошибка при создании значения даты");
        }

    }
}
