
package test;

import java.text.ParseException;
import java.util.Date;
import java.util.Random;


/** Перечисление с указанными типами
 * 
 * @author Nold
 */
enum TypeEnum { 

    INTEGER { 
        public Object parse(String string) { return Integer.valueOf(string); } 
    }, 
    STRING { 
        public Object parse(String string) { return string; } 
    }, 
    FLOAT {
        public Object parse(String string) { return Float.valueOf(string); } 
    },
    DATE {
        Date date;
        public Object parse (String string) throws ParseException {
            try {
                this.date=DataCheck.toDate(string, "dd.MM.yyyy");
            } catch (ParseException ex) {
                throw ex; 
            }
        return false;
        }
    };
  
    /**
     * метод получения случайного типа из перечисления
     * @return возвращает имя типа из перечисления
    */
    public static String getRandomType () {
        Random r = new Random ();
        TypeEnum[] types = TypeEnum.values();
        TypeEnum type = types[r.nextInt(TypeEnum.values().length)];
        return (type.toString());   
    }
    
    public abstract Object parse(String string) throws ParseException;

} 
