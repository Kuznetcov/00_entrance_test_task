package test;
import com.beust.jcommander.JCommander;

/** Основной класс приложения генератор тестовых данных (CSV файл)
 * Программа генерирующая тестовые данные для программы «Поиск по CSV файлу» с
 * заданным количеством колонок и строк.
 * @author Nold
 */
public class GenTest {

    /**
     *  Запуск приложения из консоли. Передаваемые параметры:
     * -col количество колонок csv файла Default: 5
     * -len максимальная длина String элементов csv файла Default: 25
     * -out имя выходного файла Default: result.csv
     * -row количество строк csv файла Default: 10
     * -help, -? вывод параметров
     */
    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        Param comLine = new Param();
        JCommander jc = new JCommander(comLine);
        //JCommander - используется для работы с командной строкой
        try {   //проверка параметров
            jc.parse(args);

            if (comLine.isHelp()) {
                jc.usage();
            }
            System.out.printf("Полученные параметры Количество столбцов %d \n"
                    + "Количесто строк %d \n"
                    + "Длина строки %d \n"
                    + "Выходной файл %s \n",
                    comLine.getCol(), comLine.getRow(), comLine.getLen(), comLine.getOut()
            );
            

        } catch (Exception t) {
            System.out.println("Параметры введены неправильно. Для справки используйте -help");
            System.exit(0);
        }
        
        try {   //создание файла
            GenFile.genFile(comLine);
            long timeSpent = System.currentTimeMillis() - startTime;
            System.out.println("программа выполнялась " + timeSpent + " миллисекунд");
        } catch (Exception x) {
            System.out.println(x.getMessage());
        }

    }

}
