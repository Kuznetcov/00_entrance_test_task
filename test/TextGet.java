
package test;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
/** Класс для генерации случайных строк
 * @author Nold
 */
public class TextGet {
    /**
    * метод получения случайной строки
    * @param from минимальная длина
    * @param to максимальная длина
    * @return возвращает сгенерированную строку
    */
    public static String generate(int from, int to) {
        String pass  = "";
        Random r     = new Random();
        int cntchars = from + r.nextInt(to - from + 1);

        for (int i = 0; i < cntchars; ++i) {
            char next = 0;
            int range = 10;

            switch(r.nextInt(3)) {
                case 0: {next = '0'; range = 10;} break;
                case 1: {next = 'a'; range = 26;} break;
                case 2: {next = 'A'; range = 26;} break;
            }

            pass += (char)((r.nextInt(range)) + next);
        }

        return pass;
    }
    
    /**
    * метод получения случайной даты
    * @param dateFromat формат даты
    * @return возвращает дату в определенном формате
    */
    public static String generateDate(String dateFromat) throws ParseException {
        long beginTime;
        long endTime;
        beginTime = Timestamp.valueOf("2014-01-01 00:00:00").getTime();
        endTime = Timestamp.valueOf("2016-01-01 00:00:00").getTime();
        SimpleDateFormat format = new SimpleDateFormat(dateFromat);
        format.setLenient(false);
        Date date = new Date(beginTime + (long) (Math.random() * (endTime - beginTime + 1)));
        return format.format(date);
    }
}
