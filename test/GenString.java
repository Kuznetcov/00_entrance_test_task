
package test;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Random;
import static test.TextGet.generate;

/** Класс для генерации строк со случайными типами.
 * В качестве имен столбцов используется ID
 * @author Nold
 */
public class GenString {

    ArrayList<Attribute> list = new ArrayList<Attribute>();
    ArrayList<String> titlenames = new ArrayList<String>();
    private static Integer ID = 0;

    /**
    * метод получения титульной строки со случайными типами
    * @param col требуемое количество колонок
    * @return возвращает титульную строку
    */
    public String[] getTitleRow(int col) {
        for (int i = 0; i < col; i++) {

            list.add(new Attribute());
            titlenames.add(list.get(i).getName() + " " + list.get(i).getType());
        }
        return (titlenames.toArray(new String[titlenames.size()]));
    }

    ;
    
    /**
    * метод получения случайной строки по шаблону требуемых типов
    * @param paramlist параметры полученные программой из командной строки
    * @return строку для записи в файл
    */
    public String[] getRndRow(Param paramlist) throws ParseException {
        Random random = new Random();
        ArrayList<String> ourrow = new ArrayList<String>();
        for (int i = 0; i < paramlist.getCol(); i++) {
            switch (list.get(i).getType()) {             
                case "INTEGER":
                    ourrow.add(Integer.toString(random.nextInt()));
                    break;
                case "FLOAT":
                    ourrow.add(Float.toString(random.nextFloat()));
                    break;
                case "STRING":
                    ourrow.add(generate(1, paramlist.getLen()));
                    break;
                case "DATE":
                    ourrow.add(TextGet.generateDate("dd.MM.yyyy"));
                    break;    
                default:
                    
                    System.out.println("Неправильный тип");
            }
        }
        return (ourrow.toArray(new String[ourrow.size()]));
    }

    /**
    * Внутренний класс-контейнер для создания колонок
    * хранит имя и тип колонки.
     * Можно получить тип и имя
     */
    private class Attribute {

        /**
         * конструктор генерации случайного типа для колонки
         *
         * @param type тип
         */
        public Attribute() {
            this.name = "ID" + (++ID);
            this.type = TypeEnum.getRandomType();
        }
        private String type;

        public String getType() {
            return type.toString();
        }

        private String name;

        public String getName() {
            return name;
        }

    }
}
