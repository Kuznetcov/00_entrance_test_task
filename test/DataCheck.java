package test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/** Класс для проверки даты
 * @author Nold
 */
public class DataCheck {
  
    /**
     * метод проверки соответствует ли дата требуемому формату
     * @param dateToValidate проверяемая дата
     * @param dateFromat формат согласно которому проверяется дата
     * @return возвращает true, если дата соотвует формату
     */
    public static boolean isDate(String dateToValidate, String dateFromat) throws ParseException {
        if (dateToValidate == null) {
            return false;
        }
        SimpleDateFormat sdf = new SimpleDateFormat(dateFromat);
        sdf.setLenient(false);
        Date date = sdf.parse(dateToValidate);
        System.out.println(date);
        return true;
    }
    
    /**
     * метод преобразования строки в дату согласно формату
     * @param dateToValidate дата
     * @param dateFromat формат даты
     * @return возвращает дату определенного формата
     */
    public static Date toDate(String dateToValidate, String dateFromat) throws ParseException   {
        
            SimpleDateFormat format = new SimpleDateFormat(dateFromat);
            format.setLenient(false);
            Date docDate= format.parse(dateToValidate);
            return docDate;
    }
}
