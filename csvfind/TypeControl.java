package csvfind;

import static csvfind.TypeEnum.*;

/**
 * Класс проверки корректности типов
 *
 * @author Nold
 */
public class TypeControl {

    /**
     * метод сравнения хранимого в выражении и типа столбца
     *
     * @param val значение в строке
     * @param tip тип столбца
     * @return истина, если тип совпадает
     */
    public static boolean typeControl(String val, String tip) {

        if (val.length() != 0) {
            try {
                switch (tip) {
                    case "Integer":
                    case "INTEGER":
                        INTEGER.parse(val);
                        break;
                    case "Float":
                    case "FLOAT":
                        FLOAT.parse(val);
                        break;
                    case "String":
                    case "STRING":
                        STRING.parse(val);
                        break;
                    case "Date":
                    case "DATE":
                        DATE.parse(val);
                        break;
                    default: {
                        System.out.println("Неправильный тип");
                        return true;
                    }
                }
            } catch (Exception x) {
                System.out.println("Тип и содержимое не соответствуют");
                return true;
            }
        }
        return false;
    }
}
