package csvfind;

import com.opencsv.CSVWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

/**
 * Класс для создания выходного файла CSV
 *
 * @author Nold
 */
class FileCreate {

    /**
     * метод проверки соответствует ли дата требуемому формату
     *
     * @param out путь-имя выходного файла
     * @param csv строки для записи
     * @param enc кодировка выходного файла
     */
    public static void resultCSV(String out, ArrayList<String[]> csv, String enc) throws IOException {

        try (CSVWriter writer = new CSVWriter(new OutputStreamWriter(new FileOutputStream(out), enc), ';');) {
            writer.writeNext(HeadFind.headOut);
            for (int i = 0; i < csv.size(); i++) {
                String[] nextLine = csv.get(i);
                writer.writeNext(nextLine);
            }
        }
    }
}
