
package csvfind;

/** Класс работы с csv строками, контейнер для строки заголовков
 * @author Nold
 */
public class HeadFind {

    /**
     * конструктор класса, определяет полное название столбца для поиска и
     * тип содержащийся в столбце
     * @param head полученное из командной строки имя столбца
     * @param hf первая строка файла с именами столбцов
     */        
    public HeadFind(String head, String[] hf) throws Exception {
        try {
            headFull = getHeadFull(head, hf);
            headName = head;
            headType = colType(headFull);
        } catch (Exception x) {
            throw x;
        }
    }

    public String headName;
    public String headType;
    public String headFull;
    public static String[] headOut;

     /**
     * метод поиска столбца
     * @param head проверяемая дата
     * @return номер столбца с именем
     */    
    public int colNum(String[] head) throws Exception {
        int num = -1;
        for (int i = 0; i < head.length; i++) {
            if (headFull.equals(head[i])) {
                num = i;
            }
        }
        if (num == -1) {
            throw new Exception("Во входящем файле указанный столбец не найден");
        } else {
            headOut = head;
            return num;
        }
    }
    
    /**
     * метод получения типа столбца
     * @param head полное имя столбца
     * @return тип в столбце
     */   
    private String colType(String head) throws Exception {
        if (head.indexOf(' ') != head.lastIndexOf(' ')) {
            throw new Exception("Неправильные колонки файла");
        } else {
            return (head.substring(head.indexOf(' ') + 1));
        }
    }

    /**
     * метод получения имени и типа искомого столбца
     * @param head только имя столбца без типа
     * @param head полная строка, которая возможно содержит имя столбца
     * @return тип в столбце
     */   
    private String getHeadFull(String head, String[] hf) throws Exception {
        
        for (String hf1 : hf) {
            //имена колонок должны содержать только один пробел,
            //разделяющий имя и тип столбца
            if (!hf1.contains(" ")
                    || hf1.indexOf(' ') != hf1.lastIndexOf(' ')
                    || hf1.indexOf(' ') == -1) {
                System.out.println(head+"|"+hf1);
                throw new Exception("Неправильные названия колонок файла. Формат: Имя Тип ");
            } else if (hf1.substring(0,hf1.indexOf(' ')).equals(head)) {
                return (hf1);
            } 
        }
        throw new Exception ("Искомая колонка в файле не найдена");
    }
}
