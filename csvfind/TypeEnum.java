package csvfind;

import java.text.ParseException;
import java.util.Date;


/** Перечисление с указанными типами
 * Добавлены типы не указанные в тех задании. Эти типы лишние их надо бы удалить
 * @author Nold
 */
enum TypeEnum { 

    INTEGER { 
        public Object parse(String string) { return Integer.valueOf(string); } 
    },
	CHAR { 
        public Object parse(String string) { return string[0]; } 
    },
	DOUBLE {
		public Object parse(String string) { return Double.valueOf(string); }
	},
    STRING { 
        public Object parse(String string) { return string; } 
    }, 
    FLOAT {
        public Object parse(String string) { return Float.valueOf(string); } 
    },
    DATE {
        Date date;
        public Object parse (String string) throws ParseException {
            try {
                this.date=DataCheck.toDate(string, "dd.MM.yyyy");
            } catch (ParseException ex) {
                throw ex; 
            }
        return false;
        }
    };

    public abstract Object parse(String string) throws ParseException;

} 
