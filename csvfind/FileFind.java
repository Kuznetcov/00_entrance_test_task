
package csvfind;

import com.opencsv.CSVReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;

/** Класс для поиска в файле искомого выражения
 * используется opencsv CSVReader для чтения и преобразования csv строк
 * @author Nold
 */
public class FileFind {
    /**
     * метод для получения строк содержащих искомое выражение
     * @param paramlist список параметров полученных основной программой
     * @return лист строк содержащих искомое выражение
     */
    public ArrayList<String[]> arrGet(Param paramlist) throws FileNotFoundException, Exception {

        try (CSVReader reader = new CSVReader(new FileReader(paramlist.getIn()), ';');) {
        String[] nextLine;
        ArrayList<String[]> outStrings = new ArrayList<String[]>();
        nextLine = reader.readNext(); //получение первой строки csv файла для проверки
        
        
            HeadFind hf = new HeadFind(paramlist.getCol(), nextLine);
            if (TypeControl.typeControl(paramlist.getExp(), hf.headType)) {
                throw new Exception("Столбец и выражение для поиска не соответствуют");
            }
            int num = hf.colNum(nextLine);     //определение номера столбца

            //поиск в столбце выражения
            while ((nextLine = reader.readNext()) != null) {
                if (TypeControl.typeControl(nextLine[num], hf.headType)) {
                    throw new Exception("Неправильное значение в входном csv файле");
                }
                if (nextLine[num].equals(paramlist.getExp())) {
                    outStrings.add(nextLine);
                }
            }
            reader.close();
            if (outStrings.isEmpty()) {
                throw new Exception("Искомое выражение в файле не найдено");
            }
            return outStrings;
        } catch (Exception x) {
            throw x;
        }
    };
}
