
package csvfind;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

/** Основной класс приложения по поиску в CSV файле
 * Приложение работает с входным файлом в формате CSV (Comma Separated Values).
 * В файле содержатся табличные данные. Приложение позволяет осуществлять поиск значению в указанной колонке. 
 * Результат поиска выводится в выходной файл в формате CSV.
 * @author Nold
 */
public class CSVFind {

    /**
     *  Запуск приложения из консоли. Передаваемые параметры:
     * -col имя столбца для поиска csv файла
     * -enc кодировка выходного csv файла
     * -exp выражение для поиска
     * -in имя входного файла
     * -out имя выходного файла
     * -help, -? вывод параметров
     */
    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        Param comLine = new Param();    
       // String[] argv = {"-in", "C:/Java/Test/result.csv","-col","ID2","-exp","I2i\"n32YI4","-enc","UTF-8","-out","C:/Java/Test/out.csv"};
        //JCommander - используется для работы с командной строкой
        JCommander jc = new JCommander(comLine);
        try {
            jc.parse(args);
            if (comLine.isHelp() || args.length == 0) {
                jc.usage();
                System.exit(0);
            }
            System.out.printf("Полученные параметры %s %s %s %s %s \n",
                    comLine.getCol(), comLine.getExp(), comLine.getEnc(),
                    comLine.getOut(), comLine.getIn());
            //проверка соответствия формата входного файла
            if (comLine.getIn().lastIndexOf('.') == -1 || !".csv".equals(comLine.getIn().substring(comLine.getIn().lastIndexOf('.')))) {
                System.out.println ("Неправильный формат файла");
                System.exit(0);
            }

        } catch (ParameterException t) {
            System.out.println ("Параметры введены неправильно. Для справки используйте -help");
            System.exit(0);
        }
        
        //поиск в файле искомого выражения
        FileFind nf = new FileFind();
        ArrayList<String[]> outStrings = new ArrayList<String[]>();
        try {
            outStrings.addAll(nf.arrGet(comLine));
        } catch (FileNotFoundException x) {
            System.out.println("Указанный файл не найден");
            System.exit(0);
        } catch (Exception x) {
            System.out.println(x.getMessage());
            System.exit(0);
        }
        
        //создание выходного файла
        try {
            FileCreate.resultCSV(comLine.getOut(), outStrings, comLine.getEnc());
        } catch (IOException x) {
            System.out.println("Не удалось создать файл. Проверьте параметры");
        }
        
        long timeSpent = System.currentTimeMillis() - startTime;
        System.out.println("программа выполнялась " + timeSpent + " миллисекунд");
    
    }
}
