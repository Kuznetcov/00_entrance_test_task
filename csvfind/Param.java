
package csvfind;

import com.beust.jcommander.Parameter;
/** Класс параметров для JCommander
 *
 * @author Nold
 */
public class Param {

    @Parameter(names = { "-help", "-?" }, help = true) private boolean help;

    public boolean isHelp() {
        return help;
    }
    
    @Parameter(names = { "-col" }, description = "имя столбца для поиска csv файла") private String col;
    @Parameter(names = { "-exp" }, description = "выражение для поиска csv файла") private String exp;
    @Parameter(names = { "-enc" }, description = "кодировка csv файла") private String enc = "UTF-8";
    @Parameter(names = { "-out" }, description = "имя выходного файла") private String out = "result.csv";
    @Parameter(names = { "-in" }, description = "имя входного файла") private String in;
        

    public String getCol() {
        return col;
    }

    public String getExp() {
        return exp;
    }
    
    public String getEnc() {
        return enc;
    }    

    public String getIn() {
        return in;
    }

    public String getOut() {
        return out;
    }
}
